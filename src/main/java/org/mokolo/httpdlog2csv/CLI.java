/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.httpdlog2csv;

import java.io.BufferedReader;


import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.apache.logging.log4j.core.config.NullConfiguration;
import org.mokolo.commons.cli.CommandDefinition;
import org.mokolo.commons.cli.CommandParser;
import org.mokolo.commons.io.httpdlog.ApacheCombinedLogParser;
import org.mokolo.commons.io.httpdlog.HttpdLogEntry;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryCSVSerializer;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryMatcherImpl;
import org.mokolo.commons.io.httpdlog.HttpdLogParser;
import org.mokolo.commons.io.log4j.Log4JConfiguration;
import org.mokolo.commons.io.rrr.HttpMethod;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryCSVSerializer.Column;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryCSVSerializer.CsvFormat;
import org.mokolo.commons.lang.DateFormat;
import org.mokolo.commons.lang.document.Document;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class CLI {
  public static void main(String[] args) {

    try {
      CommandParser commandParser = new CommandParser(new OptionParser("v"), "request");
      commandParser.addCommandDefinition(new CommandDefinition("convert",  "c")
          .addOptionalArgument("inetadress")
          .addOptionalArgument("http-method")
          .addOptionalArgument("path")
          .addOptionalArgument("status")
          .addOptionalArgument("not-status")
          .addOptionalArgument("select")
          .addOptionalArgument("timestamp-format")
          .addOptionalArgument("csv-format"));
      OptionSet options = commandParser.getOptionParser().parse(args);

      Log4JConfiguration.Level level = Log4JConfiguration.Level.NORMAL;
      if (options.has("v"))
        level = Log4JConfiguration.Level.VERBOSE;
      Log4JConfiguration.setupRootLogger(level, 0);
      
      log.debug("Debug");
      log.info("Info");
      log.warn("Warn");
      log.error("Error");
      log.fatal("Fatal");
      
      Document document = commandParser.parseCommandWithSwitches(options);
      commandParser.validateDocument(document);
      
      if (document.getString("request").equals("convert")) {
        
        /*
         * Setup matcher:
         */
        HttpdLogEntryMatcherImpl matcher = new HttpdLogEntryMatcherImpl();
        
        List<String> inetAddresses = document.getStrings("inetaddress");
        if (inetAddresses != null)
          for (String address : inetAddresses)
            matcher.addInetAddress(Pattern.compile(address));
        
        List<String> httpMethods = document.getStrings("http-method");
        if (httpMethods != null)
          for (String httpMethod : httpMethods)
            matcher.addHttpMethod(HttpMethod.valueOf(httpMethod.toUpperCase()));
        
        List<String> paths = document.getStrings("path");
        if (paths != null)
          for (String path : paths)
            matcher.addPath(Pattern.compile(path));
        
        List<Integer> statuses = document.getIntegers("status");
        if (statuses != null)
          for (Integer status : statuses)
            matcher.addStatus(status);

        List<Integer> notStatuses = document.getIntegers("not-status");
        if (notStatuses != null)
          for (Integer status : notStatuses)
            matcher.addNotStatus(status);

        /*
         * Setup serializer and open:
         */
        HttpdLogEntryCSVSerializer serializer = new HttpdLogEntryCSVSerializer(System.out);
        
        List<String> selects = document.getStrings("select");
        if (selects != null) {
          for (String select : selects) {
            Column column = Column.fromName(select);
            if (column != null)
              serializer.selectColumn(column);
            else
              log.error("--select column '"+select+"' unknown");
          }
        }
        else
          serializer.selectAll();
        
        String timestampFormatString = document.getString("timestamp-format");
        if (timestampFormatString != null) {
          DateFormat dateFormat = DateFormat.valueOf(timestampFormatString.toUpperCase());
          if (dateFormat == null)
            log.error("--timestamp-format format '"+timestampFormatString+"' unknown");
          else
            serializer.setTimestampFormat(dateFormat);
        }
        
        String csvFormatString = document.getString("csv-format");
        if (csvFormatString != null) {
          CsvFormat csvFormat = CsvFormat.valueOf(csvFormatString);
          if (csvFormat == null)
            log.error("--csv-format format '"+csvFormatString+"' unknown");
          else
            serializer.setCsvFormat(csvFormat);
        }
        
        serializer.open();
        serializer.printHeader(); // Does nothing here
        
        /*
         * Setup parser:
         */
        HttpdLogParser parser = new ApacheCombinedLogParser();
        
        /*
         * Parse input lines, filter and send to serializer
         */
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null && line.length() != 0) {
          try {
            HttpdLogEntry entry = parser.parseLine(line);
            if (matcher.matches(entry))
              serializer.printLogEntry(entry);
          } catch (ParseException e) {
            log.error(e.getMessage());
          }
        }
        
        /*
         * Close serializer:
         */
        serializer.printFooter(); // Does nothing here
        serializer.close();

      }
    } catch (Exception e) {
      System.err.println(e.getClass().getName()+" occurred. Msg="+e.getMessage());
      e.printStackTrace();
    }
  }


}
