# HTTPDLOG2CSV

## Purpose

Converts Apache Combined Log Format to Comma Separated Values (CSV).
Can filter lines on several aspects.
Can restrict output to a selection of fields.

## Usage

``<command> {convert | c} [options]``

Where ``<command>`` is ``java -jar httpdlog2csv-<version>-jar-with-dependencies.jar`` or a script.

Reads input from stdin and sends output to stdout.

Use tools from package [csvkit](https://csvkit.readthedocs.io) 
to further process the output on the command line and generate tables you can
include in a Markdown document.

With no options, all lines are converted and the following fields will be available in the output:
- InetAddress (ipv4 or ipv6 address)
- Timestamp (as yyyy-MM-dd HH:mm:ss)
- HttpMethod (GET, PUT, HEAD, DELETE etc)
- Path (path including request parameters, i.e. /cars?color=black)
- Protocol (i.e. HTTP/2.0)
- Status (200, 404 etc)
- Size (in bytes)
- Referer (null if not provided)
- UserAgent (null if not provided)

## Options to filter lines

### ``--inetaddress=<regex>``

One or more occurrences of option ``--inetaddress`` restricts lines to only those matching one of the regular expressions for ``inetaddress``.
The ``<regex>`` uses the Java Regular Expressions format as described in the [Pattern class](https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html).

### ``--http-method=<method>``

One or more occurrences of option ``--http-method`` restricts lines to only those matching the given method (case-insensitive).

Example: ``--http-method=GET --http-method=POST`` restricts lines to only GET and POST requests.

Options for ``<method>``:
- DELETE
- GET
- HEAD
- OPTIONS
- POST
- PUT

### ``--path=<regex>``

One or more occurrences of option ``--path`` restricts lines to only those matching one of the regular expressions for ``path``.
The ``<regex>`` uses the Java Regular Expressions format as described in the [Pattern class](https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html).

### ``--status=<status>``

One or more occurrences of option ``--status`` restricts lines to only those matching the given statuses (Integer).
Do not use together with option ``--not-status``.

Example: ``--status=200 --status=301`` restricts lines to only status OK and Moved Permanently.

### ``--not-status=<status>``

One or more occurrences of option ``--not-status`` restricts lines to only those matching none of the given statuses (Integer).

Example: ``--not-status=200`` restricts lines to all but status OK.

## Filter fields

### ``--select=<field>``

One or more occurrences of option ``--select`` restricts fields to only those listed, and in the order listed (case insensitive).

Example: ``--select=Timestamp --select=Status`` makes ``httpdlog2csv`` only output fields ``Timestamp`` and ``Status``, in that order.

Options for ``<field>``:
- InetAddress
- Timestamp
- HttpMethod
- Path
- Protocol
- Status
- Size
- Referer
- UserAgent

## Options to change output formats

### ``--timestamp-format=<format>``

Use the given format name for the timestamp (case insensitive).

Options for ``<format>``:
- APACHE (format used in log file) 
- DATE ("yyyy-MM-dd")
- DATETIME ("yyyy-MM-dd HH:mm:ss")
- DEFAULT ("yyyy-MM-dd HH:mm:ss")
- ISO ("yyyy-MM-dd'T'HH:mm:ssZ")

With no ``--timestamp-format`` option given, DEFAULT is used.

Options DATE, DATETIME and DEFAULT all show local dates and times.

### ``--csv-format=<format>``

use the given format name for the CSF format (case insensitive).

Options for ``<format>``:
- DEFAULT (RFC4180 with comma as field separator)
- EXCEL (separator depends on locale settings)

## Example

Assuming the executable jar (with dependencies) is named ``httpdlog2csv.jar``.

```
$ ssh <host> "gunzip -c /var/log/apache2/access.log.1.gz" \
  | java -jar httpdlog2csv.jar \
    convert --select=protocol \
  | csvsql --tables data --query \
    "select Protocol, count(*) from data group by Protocol" \
  | csvlook 
| Protocol | count(*) |
| -------- | -------- |
| HTTP/1.0 |        2 |
| HTTP/1.1 |      285 |
```
